<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Account;

class GlobalController extends Controller
{
    public function index(Request $request, $id = "")
    {
        if ($id == "") {
            //$request->session()->forget('url');
            return view('template', compact('id'));
        } else {
            //redirect to the url here
            //update increment
            $account = Account::where('uniqid', $id)->first();

            if ($account) {
                if ($account->url != "" && $account->url != null) {
                    $account->views = ($account->views == null) ? 1 :  $account->views + 1;
                    $account->save();
                    if ($account->tiktok == 1)
                        return redirect('https://www.tiktok.com/@' . $account->url);
                    else if ($account->snaptchat == 1)
                        return redirect('https://www.snapchat.com/add/' . $account->url);
                    else if ($account->instagram == 1)
                        return redirect('https://instagram.com/' . $account->url);
                } else {
                    return view('template', compact('id'));
                }
            } else {
                $request->session()->flash('invalid', 'Sorry the QrCode you are trying to use do not exist');
                return view('template', 'id');
            }
        }
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:Account,url|max:255',
        ]);
        $id = $request->get('uid');

        if ($validator->fails()) {
            return redirect('scan/' . $id)
                ->withErrors($validator)
                ->withInput();
        }

        $type = $request->get('type');

        if ($id) {
            $account = Account::where('uniqid', $id)->first();
            $account->url = $request->get('username');
            if ($type == "tiktok")
                $account->tiktok = 1;
            if ($type == "snaptchat")
                $account->snaptchat = 1;
            if ($type == "instagram")
                $account->instagram = 1;
            $account->save();
        } else {
            $account = new Account();
            $account->url = $request->get('username');
            if ($type == "tiktok")
                $account->tiktok = 1;
            if ($type == "snaptchat")
                $account->snaptchat = 1;
            if ($type == "instagram")
                $account->instagram = 1;
            $account->save();
        }

        $request->session()->flash('status', 'Your Qr Code is now active, you can share it around !!');
        $request->session()->flash('url', $account->uniqid);

        return redirect('scan');
    }

    public function qrcode()
    {
        $accounts = Account::paginate(100);
        return view('scanner', compact('accounts'));
    }

    public function generate(Request $request)
    {
        $accounts = Account::all();

        if (count($accounts) <= $request->get('generate')) {
            $total = $request->get('generate') - count($accounts);
            for ($i = 0; $i < $total; $i++) {
                $account = new Account();
                $account->save();
            }
        } else {
            $total = $request->get('generate');
            for ($i = 0; $i < $total; $i++) {
                $account = new Account();
                $account->save();
            }
        }

        return redirect('qrcode');
    }
}