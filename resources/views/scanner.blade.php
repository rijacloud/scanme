<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scan Me If You Can</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic"
        rel="stylesheet" type="text/css">
    <link href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('css/stylish-portfolio.min.css') }}" rel="stylesheet">

    <style>
    .alert {
        max-width: 50%;
        margin: 12px auto;
        padding: 5px;
    }

    .alert ul {
        list-style: none;
        margin-bottom: 4px;
        padding-left: 0;
    }

    .qrcode img {
        margin: 10px;
    }
    </style>

</head>

<body id="page-top">
    <script type="text/javascript" src="//stuk.github.io/jszip/dist/jszip.js"></script>
    <script type="text/javascript" src="//stuk.github.io/jszip-utils/dist/jszip-utils.js"></script>
    <script type="text/javascript" src="//stuk.github.io/jszip/vendor/FileSaver.js"></script>
    <!-- Navigation -->
    <!--a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
      <li class="sidebar-brand">
        <a class="js-scroll-trigger" href="#page-top">Start Bootstrap</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#page-top">Home</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#about">About</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#services">Services</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#portfolio">Portfolio</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#contact">Contact</a>
      </li>
    </ul>
  </nav-->

    <!-- Header -->
    <header class="masthead d-flex">
        <div class="container">
            <h1 class="mb-1">Vos Qrcodes</h1>
            <form action="{{ route('qrcode.generate') }}" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                    <input name="generate" class="form-control" placeholder="Entrez le nombre de QrCode à générer" />
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Générer Qrcode</button>
                </div>
            </form>
            <button class="btn btn-primary" onclick="download()" style="margin:20px auto;display:block;">Téléchager les
                QR
                codes</button>
            <div class="qrcode" style="max-height: 1px;overflow:hidden;">
                @foreach ($accounts as $account)
                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(250)
                        ->generate('http://scanmeifyoucan.com/scan/'.$account->uniqid)) !!} " class="qrcd">
                @endforeach

                {{$accounts->links()}}
            </div>
        </div>
        <div class="overlay"></div>
    </header>


    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{ asset('js/stylish-portfolio.min.js') }}"></script>
    <script>
    function getBase64Image(img) {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    }

    function download() {
        var imgs = document.querySelectorAll('.qrcd')
        var zip = new JSZip();
        var img = zip.folder("images");
        var counter = 0
        imgs.forEach(function(item) {
            var i = getBase64Image(item)
            img.file(counter + ".png", i, {
                base64: true
            });
            counter = counter + 1
        })

        zip.generateAsync({
            type: "blob"
        }).then(function(content) {
            saveAs(content, "edm8.zip");
        });

    }
    </script>
</body>

</html>