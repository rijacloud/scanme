<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('scan');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Route::get('/scan/{id?}', 'App\Http\Controllers\GlobalController@index')->middleware('web');
Route::post('/scan/{id?}', 'App\Http\Controllers\GlobalController@create')->middleware('web')->name('qrcode.create');

Route::get('/qrcode', 'App\Http\Controllers\GlobalController@qrcode')->middleware('auth');
Route::post('/qrcode', 'App\Http\Controllers\GlobalController@generate')->middleware('auth')->name('qrcode.generate');